import { mount } from "@vue/test-utils";
import Vue from "vue";
import GridTabelaValores from "@/components/GridTabelaValores.vue";

describe('GridTabelaValores.vue', () => {
    it('renders props.planos when passed', () => {
        const planos = []
        const wrapper = mount(GridTabelaValores, {
            propsData: {
                planos: planos
            }
        })
        expect(wrapper.is(typeof (Array)))
    })

    it('render props with correct type', () => {
        const Constructor = Vue.extend(GridTabelaValores)
        const vm = new Constructor({ propsData: [] }).$mount()
    })

})
