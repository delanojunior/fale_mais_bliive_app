import { mount } from "@vue/test-utils";
import SimuladorPlanoForm from "@/components/SimuladorPlanoForm.vue";


describe('SimuladorPlanoForm', () => {
    it('test button simular emit plano_simulado', () => {

        const wrapper = mount(SimuladorPlanoForm)

        wrapper.setData({
            plano: {
                origem: 11,
                destino: 17,
                tipo_plano: "Fale mais 30",
                tempo: 15
            },
        })
        wrapper.find('v-btn.btnSimular').trigger('click')
        expect(wrapper.emitted().plano_simulado).toBeTruthy()
    })

    it('test selects preenchidos', () => {
        const wrapper = mount(SimuladorPlanoForm, {
            props: {
                planos: ['FaleMais 30', 'FaleMais 60', 'FaleMais 120']
            }
        })

        expect(wrapper.props('planos')).toHaveLength(3)
    })
})
