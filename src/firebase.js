import Firebase from "firebase";

const firebaseApp = Firebase.initializeApp({
    apiKey: "AIzaSyDVDjMIymsUqkLiEnzl6wC-cKyR9oGctfc",
    authDomain: "falamais-backend.firebaseapp.com",
    databaseURL: "https://falamais-backend.firebaseio.com",
    projectId: "falamais-backend",
    storageBucket: "falamais-backend.appspot.com",
    messagingSenderId: "810232101789"
})

export const firebaseDB = firebaseApp.database()