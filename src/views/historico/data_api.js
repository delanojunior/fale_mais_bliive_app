import { firebaseDB } from "../../firebase";

export class DataAPIHistorico {

    fetchHistorico() {
        let historico = []
        firebaseDB.ref('planos').on('value', (data, erro) => {
            if (erro) {
                console.log(erro)
                return;
            }

            let historico_array = data.val()
            let historcio_mapped = Object.keys(historico_array).map(h => historico_array[h])

            historcio_mapped.forEach(h => {
                let historico_plano = {
                    'data': h.data,
                    'origem': h.origem,
                    'destino': h.destino,
                    'tempo': h.tempo,
                    'tipo_plano': h.tipo_plano,
                    'preco_com_falamais': h.preco_com_falamais,
                    'preco_sem_falamais': h.preco_sem_falamais
                }
                historico.push(historico_plano)
            })
        })
        return historico
    }

    pushHistorico(plano_simulado) {
        let historico_plano = {
            'destino': plano_simulado.destino,
            'origem': plano_simulado.origem,
            'preco_com_falamais': plano_simulado.preco_com_falamais,
            'preco_sem_falamais': plano_simulado.preco_sem_falamais,
            'tempo': plano_simulado.tempo,
            'tipo_plano': plano_simulado.tipo_plano,
            'data': new Date().toJSON()
        }
        firebaseDB.ref('planos').push(historico_plano)
    }

}