import HistoricoSimulacoesPlanos from "./HistoricoSimulacoesPlanos.vue";

export default [
    {
        path: '/historico',
        name: 'historico_simulacoes',
        component: HistoricoSimulacoesPlanos
    }
]