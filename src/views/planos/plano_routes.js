import SimuladorPlano from "./SimuladorPlano.vue";

export default [
    {
        path: '/simulador',
        name: 'simulador_plano',
        component: SimuladorPlano
    },
]