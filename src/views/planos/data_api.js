export class DataAPI {

    fetchPlanosData() {
        return [
            {
                origem: 11,
                destino: 16,
                minuto: 1.90
            },
            {
                origem: 16,
                destino: 11,
                minuto: 2.90
            },
            {
                origem: 11,
                destino: 17,
                minuto: 1.70
            },
            {
                origem: 17,
                destino: 11,
                minuto: 2.70
            },
            {
                origem: 11,
                destino: 18,
                minuto: 0.90
            },
            {
                origem: 18,
                destino: 11,
                minuto: 1.90
            }
        ]
    }

    fetchOrigensEDestinos = () => {
        return [
            11, 16, 17, 18
        ]
    }

    fetchTiposPlano = () => {
        return [
            { id: 1, descricao: 'Fale mais 30', tempo: 30 },
            { id: 2, descricao: 'Fale mais 60', tempo: 60 },
            { id: 3, descricao: 'Fale mais 120', tempo: 120 }
        ]
    }


    calcularTempoComPlano(minuto_tipo_plano, qtdMinutos, minuto_plano) {

        if (minuto_tipo_plano == 30 && qtdMinutos < minuto_tipo_plano)
            return 0.0

        let tempoSemAcrescimo = qtdMinutos - minuto_tipo_plano;
        console.log(tempoSemAcrescimo)
        let tempoMutiplicatoPeloPlano = tempoSemAcrescimo * minuto_plano
        let porcentagemAcrescimo = tempoSemAcrescimo * (minuto_plano * 0.10)
        let tempo = tempoMutiplicatoPeloPlano + porcentagemAcrescimo
        return tempo;
    }
}