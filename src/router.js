import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/views/home/Home'
import planos_routes from "@/views/planos/plano_routes";
import historico_routes from "@/views/historico/historico_routes";

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    { path: '/', redirect: '/home', },
    { path: '/home', component: Home },
    ...planos_routes,
    ...historico_routes
  ]
})
